﻿using System;
using System.Collections.Generic;

namespace xls2text
{
    class Program
    {
        static void TryParseRowModel(string path, ref List<RowModel> list)
        {
            using (var sr = new System.IO.StreamReader(path))
            {
                string line;
                bool isFirst = true;
                while ((line = sr.ReadLine()) != null)
                {
                    if (isFirst)
                    {
                        isFirst = false;
                        continue;
                    }

                    line = line.Replace("\n", "").Replace(Environment.NewLine, "");
                    var vals = line.Split(",");
                    if (vals.Length != 5)
                        continue;

                    list.Add(new RowModel()
                    {
                        TestNo = vals[0],
                        Class = vals[1],
                        ClassDescription = vals[2],
                        Function = vals[3],
                        FunctionDescription = vals[4],
                    });
                }
            }
        }
        static void Main(string[] args)
        {
            List<RowModel> list = new List<RowModel>();
            TryParseRowModel("/home/ieg/myfs/test/xls2text/test.csv", ref list);

            if (System.IO.File.Exists("/home/ieg/myfs/test/xls2text/test.txt"))
                System.IO.File.Delete("/home/ieg/myfs/test/xls2text/test.txt");
            foreach (var row in list)
            {
                using (var sw = new System.IO.StreamWriter("/home/ieg/myfs/test/xls2text/test.txt", true))
                {
                    sw.Write($"{row.ToString()}");
                }
            }
        }
    }
}
