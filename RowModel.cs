using System;

public class RowModel
{
    public string TestNo { get; set; }
    public string Class { get; set; }
    public string ClassDescription { get; set; }
    public string Function { get; set; }
    public string FunctionDescription { get; set; }

    public override string ToString()
    {
        return $"{this.Class}: {this.ClassDescription}\t[ {this.TestNo} ]: {this.Function}: {this.FunctionDescription}\n";
    }
}